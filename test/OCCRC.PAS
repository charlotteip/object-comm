uses OcUtils;

const
  BufSize = 60000;
var
  F : file;
  Buf : array[1..BufSize] of byte;
  Got : word; { # of bytes in current chunk }

  lCrc : longint;
  wCrc : word;

begin
  {- Title/help -}
  Writeln('Object Comm FIle Crc Checker');
  if ParamCount = 0 then begin
    Writeln('Usage: OcCrc <filename>');
    Halt(1);
  end; { then }

  {- Init -}
  Got := BufSize;
  lCrc := $FFFFFFFF;
  wCrc := 0;
  Assign(F, ParamStr(1));
  Reset(F, 1);
  while Got = BufSize do begin
    BlockRead(F, Buf, BufSize, Got);
    lCrc := Crc32Update(lCrc, Buf, Got);
    wCrc := Crc16Update(wCrc, Buf, Got);
  end; { while }
  Close(F);

  Writeln;
  Writeln(ParamStr(1), ' Crc16 = $', HexSt(not wCrc, 4), ' Crc32 = $', HexSt(not lCrc, 8));
end.