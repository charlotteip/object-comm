program Terminal;
{- Terminal demonstration program. ANSI, Ftp, BreakOut logging. -}

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

uses {$I CrtUnit.inc},
     OcLayers,
       OcAsync,
       OcTest,
       OcFtp,
         OcXmodem,
         OcAscFtp,
         OcZmodem,
       OcTerm,
     OcUtils;

type
  ProcessMethodT = procedure;

var

  {- Layers -}
  Async   : ComPort;
  Ansi    : AnsiTerm;

const
  BoPtr   : BreakOutPtr = nil;
  FtPtr   : FtpPtr      = nil;

var
  {- Async Parms -}
  UPort   : word;     { UART Port, if > 4 its a port address, else COM # }
  UInt    : word;     { Com Int, if specified }
  USpeed  : longint;  { Port speed to use }

  {- Terminal settings and variables -}
const
  Echo : boolean = false;    { Do we display outgoing characters locally? }

var
  Busy    : boolean;         { Main Loop flag  }
  Process : ProcessMethodT;  {  Procedure to use for IO processing in main loop }
  InitOk  : boolean;         { For saving init results }


  {- Ftp stuff -}
  OldAnsiLower  : pointer;   { Saves ptr to layer below Ansi layer }
  LastPos       : longint;   { Minimizes screen updating }

  {- Breakout stuff -}
  LogName   : string[67];        { Log file name }

const
  LogPaused : boolean = false;   { True when the log is paused }



{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

  procedure StatusLine;
  {- Refreshes Status line -}
  const
    DplxAra  : array[false..true] of string[6] = ('FDX � ', 'HDX � ');
    LogAra   : array[false..true] of string[4] = ('    ', 'LOG ');
    PauseAra : array[false..true] of string[9] = ('       � ', 'PAUSED � ');
    FtpAra   : array[false..true] of string[6] = ('    � ', 'FTP � ');
  var
    OTa, Ox, Oy : byte;
  begin
    OTa := TextAttr; Ox := WhereX; Oy := WhereY;

    Window(1, 1, 80, 25);
    GotoXY(1, 25);
    TextAttr := LightGray shl 4;
    Write('�Object Comm Terminal � Alt-Z for help � ', USpeed, ' � ',
          DplxAra[Echo], LogAra[BoPtr <> nil], PauseAra[LogPaused],
          FtpAra[FtPtr <> nil]);

    ClrEol;
    TextAttr := Ota;
    Window(1, 1, 80, 24);
    GotoXY(Ox, Oy);
  end; { StatusLine }

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

  procedure Startup;
  {- Title, screen, command line, main stack -}
  begin
    {- Help? -}
    if ParamCount < 2 then begin
      Writeln(
        'Usage: terminal <COM port number> <speed>' + CRLF +
        '   or  terminal <UART address> <UART interrupt> <speed>' + CRLF);
      Halt(1);
    end; { then }

    {- Title, list parameters -}
    TextAttr := LightGray;
    Window(1, 1, 80, 25);
    ClrScr;

    {- Command line -}
    Writeln('Startup Parameters' + CRLF +
            '-----------------------------------');
    for UPort := 0 to ParamCount do
      Writeln('Parameter ', UPort, ': "', ParamStr(UPort), '"');
    Writeln;

    { Async layer }
    UInt := 0;
    UPort := DecVal(ParamStr(1));
    if UPort > 4 then begin
      UInt   := DecVal(ParamStr(2));
      USpeed := DecVal(ParamStr(3));
      InitOk := Async.InitCustom(UPort, UInt);
    end { then }
    else begin
      USpeed := DecVal(ParamStr(2));
      InitOk := Async.Init(UPort);
    end; { else }

    if not InitOk then begin
      Writeln('Async.Init failed...');
      Halt(1);
    end; { then }
    Async.ComOptions(USpeed, 8, NoParity, 1, 0);

    {- Ansi Setup -}
    if not Ansi.Init then begin
      Writeln('Ansi.Init failed...');
      Halt(1);
    end; { then }
    StackLayers(@Async, @Ansi);

    {- Activate Layers -}
    if not (Async.Activate and Ansi.Activate) then begin
      Async.Deactivate;
      Writeln('Async/Ansi Activate failed.');
      Halt(1);
    end; { then }

    {- Some modems like these signals on -}
    Async.WriteUart(MCR, Async.ReadUart(MCR) or DtrOn or RtsOn);
    StatusLine;
  end; { StartUp }

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}
  {$F+}

  procedure FtpProcess; forward;

  procedure TermProcess;
  {- Handles IO processing in normal mode -}

  var
    FtPaths : string;  { File Transfer Paths }


    {- Private utility functions used by TermProcess -}

    function GetProtocol : char;
    {- Lets user choose a protocol -}
    var C : char;
    begin
      {- Pick a protocol -}
      Write(CRLF + CRLF +
        '1. ASCII            4. Ymodem' + CRLF +
        '2. Xmodem           5. Ymodem-G' + CRLF +
        '3. XModem-1k        6. Zmodem' + CRLF + CRLF +

        'Pick an upload protrocol, or "0" to cancel:');
        repeat C := ReadKey until C in ['0'..'6'];
      GetProtocol := C;
    end; { GetProtocol }

    {. . . . . . . . . . . . .  . . . . . }

    function ConstFtp(C : char) : boolean;
    {- Constructs correct Ftp object using FtPtr -}
    begin

      {- Construct objects -}
      case C of
        { ASCII }
        '1' : New(AscFtpPtr(FtPtr), Init);

        { Xmodem variants }
        '2'..'5' : New(XmodemPtr(FtPtr), Init);

        { Zmodem  }
        '6' : New(ZmodemPtr(FtPtr), Init);
      end; { case }
      if FtPtr = nil then begin
        Writeln(CRLF + '*** Construction of Ftp object failed! ***' + CRLF);
        ConstFtp := false;
      end { then }
      else ConstFtp := true;
    end; { ConstFtp }

    {. . . . . . . . . . . . .  . . . . . }

    procedure SetFtpOptions(Dir, Mode : char);
    {- Set up options for eeach file transfer modde or type. Dir is U for
       upload, D for download. Mode is the file transfer numbers used by
       SetDL/UL procedures. -}
    var C : char;
    begin
      case Mode of
        { ASCII }
        '1' : begin
          Write(CRLF + 'Pace character?');
          Read(C);
          Writeln;
          if C <> CR then
            AscFtpPtr(FtPtr)^.AscFtpOptions(Yes, C, 0, 5, CRLF, CRLF, ^Z);
        end; { ASCII options }

        { Xmodem variants }
        '2'   : XmodemPtr(FtPtr)^.XmodemOptions(XmodemCrc);

        { Xmodem variants }
        '3'   : XmodemPtr(FtPtr)^.XmodemOptions(Xmodem1kCrc);

        { Xmodem variants }
        '4'   : XmodemPtr(FtPtr)^.XmodemOptions(Ymodem);

        { Xmodem variants }
        '5'   : XmodemPtr(FtPtr)^.XmodemOptions(YmodemG);

        { Zmodem  }
        '6' : { Don't set anything };
      end; { case }
    end; { SetFtpOptions }

    {. . . . . . . . . . . . .  . . . . . }

    procedure StartFtp(Dir : char);
    {- Starts up Ftp session. Dir is 'U' = upload, 'D' = download. -}
    begin

      {- Start up file transfer -}
      OldAnsiLower := Ansi.Lower;
      StackLayers(OldAnsiLower, FtPtr);
      if Dir = 'U' then FtPtr^.Transmit(FtPaths, nil)
      else FtPtr^.Receive(FtPaths, nil);
      if not FtPtr^.Activate then begin
        StackLayers(OldAnsiLower, @Ansi);
        Dispose(FtPtr, Done);
        Writeln(CRLF + '*** Activation of Ftp object failed! ***' + CRLF);
      end; { then }
      Process := FtpProcess;
      ClrScr;
      Write(
        '    Ftp Class: ' + FtPtr^.LayerClass + CRLF +
        ' Current File:' + CRLF +
        'File Progress:' + CrLf + CrLf +
        'Press <Esc> to cancel file transfer, or ^X to abort.');
      LastPos := -1;

      {- Pause logging -}
      StatusLine;
    end; { StartFtp }


    {- TermProcess command implementations -}

    procedure StartUpload;
    {- Sets up a file upload -}
    var C : char;
    begin
      C := GetProtocol;

      {- Get file names to upload -}
      if C <> '0' then begin
        Write(CRLF + CRLF +
           'Enter file(s) to send. Example "\*.bat;\utils\*.exe;"' + CRLF +
           '->');
        Readln(FtPaths);
        if FtPaths = '' then C := '0';
        Writeln;
      end; { then }
      if C = '0' then begin
        ClrScr;
        Exit;
      end; { then }

      {- Construct objects -}
      if not ConstFtp(C) then begin
        Writeln(CRLF + '*** Construction of Ftp object failed! ***' + CRLF);
        Exit;
      end; { then }

      {- Get upload options -}
      SetFtpOptions('U', C);

      {- Start up file transfer -}
      StartFtp('U');
    end; { StartUpload }


    {- - - - - - - - - - - - - - - - - - -}

    procedure StartDownLoad;
    {- Sets up a file download -}
    var C : char;
    begin
      {- Choose protocol -}
      C := GetProtocol;

      {- Get file names to upload -}
      if C <> '0' then begin
        Write(CRLF + CRLF +
           'Enter file name or path to receive to. Example "file.txt" or "\utils\new\"' + CRLF +
           '->');
        Readln(FtPaths);
        Writeln;
      end; { then }
      if C = '0' then begin
        ClrScr;
        Exit;
      end; { then }

      {- Construct objects -}
      if not ConstFtp(C) then begin
        Writeln(CRLF + '*** Construction of Ftp object failed! ***' + CRLF);
        Exit;
      end; { then }

      {- Get upload options -}
      SetFtpOptions('D', C);

      {- Start up file transfer -}
      StartFtp('D');
    end; { StartDownload }

    {- - - - - - - - - - - - - - - - - - -}

    procedure LogFileToggle;
    {- Inserts/removes a BreakOut layer underneath the Ansi Layer -}

      procedure RemoveBo;
      begin
        StackLayers(BoPtr^.Lower, @Ansi);
        Dispose(BoPtr, Done);
        BoPtr := nil;
      end; { RemoveBo }

    begin
      {- Add or remove breakout layer? -}

      {- Add layer -}
      if BoPtr = nil then begin


        {- Get file name -}

{        Write(CRLF + CRLF + 'Enter log file name ->');
        Readln(LogName);
        Writeln;
        if LogName = '' then Exit;
}
LogName := 'term.log';

        {- Create log file, insert into stack -}
        New(BoPtr, Init);
        if BoPtr = nil then begin
          Writeln('*** Unable to construct BreakOut layer ***');
          Exit;
        end; { then }

        with BoPtr^ do begin
          BreakOptions(Cyan, Yellow, [NUL..US,  #128..#255],
                       [SOH, STX, EOT, ACK, NAK, CAN], [], yes,
                       1, 10, no, yes, Logname, nil, 0);

          StackLayers(Ansi.Lower, BoPtr);
          StackLayers(BoPtr, @Ansi);
          if not Activate then begin
            Writeln('*** Unable to activate BreakOut layer ***');
            RemoveBo;
          end; { then }
{          Screen(No); }
        end; { with }
      end { then }

      {- Remove breakout layer -}
      else RemoveBo;
      LogPaused := false;
      StatusLine;
    end; { LogFileToggle }

    {- - - - - - - - - - - - - - - - - - -}

    procedure LogPauseToggle;
    {- Pauses/UnPauses disk logging -}
    begin
      if BoPtr <> nil then begin
        BoPtr^.Disk(LogPaused);  { Reverses state of pause }
        LogPaused := not LogPaused;
      end; { then }
    end; { LogPauseToggle }

  {- - - - - - - - - - - - - - - - - - -}

  var
    Tr   : char;            { For reading keyboard input }
    Size : word;            { For writing to Ansi recieve buffer }
  begin { TermProcess }

    Ansi.Process;

    {- Watch for incoming Zmodem startup -}
    while Ansi.RecvLevel >= 4 do
      if (Ansi.RecvChar = ZDLE) and
         (Ansi.RecvChar = 'B') and
         (Ansi.RecvChar = '0') and
         (Ansi.RecvChar = '0') then begin

        Writeln(CrLf + CrLf + '*** Zmodem AutoDownload detected ***' + CrLf);

        {- Construct objects -}
        if not ConstFtp('6') then
          Writeln(CRLF + '*** Construction of Zmodem object failed! ***' + CRLF)
        else begin

          {- Get download options -}
          FtPaths := '';
          SetFtpOptions('D', '6');

          {- Start up file transfer -}
          StartFtp('D');
          Exit;
        end; { else }
      end; { then }

    {- Process keystrokes -}
    while KeyPressed  do begin
      Tr := ReadKey;
      if Tr = char(0) then begin

        {- Extended keys }
        Tr := ReadKey;
        case Tr of

          { ALT-C }
          #46  : ClrScr;

          { ALT-E }
          #18  : Echo := not Echo;

          { ALT-X }
          #45  : Busy := false;

          { PgUp }
          #73  : StartUpload;

          { PgDn }
          #81  : StartDownLoad;

          { Alt-F1 }
          #104 : LogFileToggle;

          { ALT-F2 }
          #105 : LogPauseToggle;


          { Help }
          else begin
            Writeln(CRLF +
              'Terminal Commands' + CRLF +
              '--------------------------------------------------------' + CRLF +
              'ALT-C  Clear screen      PgUp   Upload (send) files'      + CRLF +
              'ALT-E  Toggle duplex     PgDn   Download (receive) files' + CRLF +
              'ALT-X  Exit program      ALT-F1 Log file On/Off toggle');
            Writeln(
              '                         ALT-F2 Log file pause toggle'    + CRLF);
          end; { else }
        end; { case }
        StatusLine;
      end { then }
      else begin
        Ansi.TranChar(Tr);
        Size := 1;
        if Echo then Ansi.RecvWrite(Tr, Size);
      end; { else }
    end; { while }
end; { TermProcess }

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

procedure FtpProcess;
begin
  with FtPtr^ do begin

    {- Aborted? -}
    while Keypressed do
      case ReadKey of
        Esc : begin
          Cancel;
          GotoXY(1, 7);
          Write('*** Canceling file transfer ***');
        end; { Esc }

        ^X  : Deactivate;
      end; { case }

    Process;

    {- Progress indicators -}
    if LastPos <> FilePos(Transmitting) then begin
      LastPos := FilePos(Transmitting);

      GotoXY(40, 1); ClrEol;
      Write('Status: ', LayerStatus);

      GotoXY(16, 2); ClrEol;
      Write(FileName(Transmitting));

      GotoXY(16, 3); ClrEol;
      Write(LastPos, '/', FileSize(Yes), '        ');
    end; { then }

    {- Are we done? -}
    if LayerStatus <= LysIdle then begin
      StackLayers(OldAnsiLower, @Ansi);
      ClrScr;
      Writeln('File transfer completed with error code #', LayerError);
      Writeln;
      Deactivate;
      Dispose(FtPtr, Done);
      FtPtr := nil;
      Terminal.Process := TermProcess;

      StatusLine;
    end; { then }
  end; { with }
end; { Ftp.Process }

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

begin { Terminal }

  {- Setup -}
  StartUp;

  {- Process -}
  FtPtr   := nil;
  BoPtr   := nil;
  Echo    := false;
  Busy    := true;
  Process := TermProcess;
  while Busy do Process;

  {- Shut down -}
  if FtPtr <> nil then begin
    FtPtr^.Deactivate;
    Dispose(FtPtr, Done);
  end; { then }

  if BoPtr <> nil then begin
    BoPtr^.Deactivate;
    Dispose(BoPtr, Done);
  end; { then }

  Ansi.Deactivate;
  Ansi.Done;

  Async.Deactivate;
  Async.Done;
end.

