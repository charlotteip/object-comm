const
  Esc     = #27;
  Lines   = 185;

  procedure GridTest;
  const
    GridE   = #0+#3;
    LineE   = #255 + #255;

  var
    Loop, Loop2, Loop3 : word;

  begin
    Write(Esc + '*t300R', Esc + '*r2400s0a0M');
    for Loop := 1 to Lines do begin
      for Loop2 := 1 to 2 do begin
        Write(Esc + '*b300W');
        for Loop3 := 1 to 150 do Write(LineE);
      end; { for }
      for Loop2 := 1 to 14 do begin
        Write(Esc + '*b300W');
        for Loop3 := 1 to 150 do Write(GridE);
      end; { for }
    end; { for }
    Write(Esc + '*rB'^L);
  end;


var
  Temp : string;
  Loop : word;

begin
  for Loop := 1 to Lines do
    Write('                                                                                '#13);
  Write(^L);
end.