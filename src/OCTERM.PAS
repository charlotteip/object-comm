{$I Switches.Inc }

  {靈컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
   �                           OcTerm.pas                        �
   �           Copyright (C) 1991, Sycamore Microsystems         �
   �                      All rights reserved.                   �
   聃컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�}

unit OcTerm;
{- Terminal objects -}

interface

uses OcLayers, OcUtils, {$I CrtUnit.inc};



{                         �袴袴袴袴袴袴袴袴敲
                          튦ABLE OF CONTENTS�
 靈컴컴컴컴컴컴컴컴컴컴컴캘                 픔컴컴컴컴컴컴컴컴컴컴컴�
 �                        �    OcTerm.pas   �                       �
 �                        훤袴袴袴袴袴袴袴袴�                       �
 � Section                                         Search for       �
 � 컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴� 컴컴컴컴컴컴컴�  �
 � TermHost (terminal host) object definition.     (below)          �
 �                                                                  �
 � AnsiTerm ANSI sequence interpreter class.       AnsiTerm Class   �
 �                                                                  �
 � TermHost class method implementations.          TermHost Methods �
 �                                                                  �
 � AnsiTerm class method implementations.          AnsiTerm Methods �
 聃컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴캭}


type

  {- TermHost defines a subset of the Crt unit API for remote terminal
     devices. It defaults to a simple BBS ANSI emulation but you may inherit
     from it to implement other terminal types. -}

  TermHostPtr = ^TermHost;
  TermHost = object(Filter)

    {- Crt unit stuff -}
    TextAttr : byte;

    constructor Init;
    {- Initializes internal layer structures. Defaults transmit and receive
       buffers to the values declared above. -}

    function TranFree : word; virtual;
    {- Reserves extra room for outgoing ANSI sequences by returning a size
       10 bytes smaller than actual buffer free. -}


    {- Methods to create transmitted terminal control sequences.
       and read incoming characters. Note that all of the standard RingBuf
       methods are also available. These methods mimic the Crt unit's
       functions. -}

    procedure GotoXY(X, Y : Byte); virtual;
    procedure ClrEol;  virtual;
    procedure ClrScr;  virtual;

    function  KeyPressed : boolean; virtual;
    function  ReadKey    : char;    virtual;

    procedure HighVideo; virtual;
    procedure LowVideo;  virtual;
    procedure NormVideo; virtual;

    procedure TextBackground(Color : Byte); virtual;
    procedure TextColor(Color : Byte);      virtual;

    procedure SendColors;
    {- Transmits current ANSI color sequence -}

  end; { TermHost }




{                         �袴袴袴袴袴袴袴袴敲
                          � AnsiTerm Class  �
 靈컴컴컴컴컴컴컴컴컴컴컴켕컴컴컴컴컴컴컴컴켕컴컴컴컴컴컴컴컴컴컴컴캠
 튍nsiTerm terminal emulation class definition.                     �
 聃컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴캭}

  {- The AnsiTerm layer class maps incoming (received) ANSI terminal escape
     sequences to Crt unit video function calls. -}

  AnsiTermPtr = ^AnsiTerm;
  AnsiTerm = object(Filter)

    {- Buffer and state information -}
    Scanner  : MethodProc;  { The scanner state method pointer }
    Waiting  : boolean;     { True when the scanner is waiting on something }

    RBuf     : array[1..128] of char;  { Buffer for incoming data }
    RHead,              { First byte in buffer }
    RTail,              { Next byte to use in buffer. }
    RScan,              { Next byte to scan for ansi sequence }
    REsc     : word;    { Current byte of escape sequence }
    EscTicks : longint; { Clock tick marker when esc char first seen }
    EscTickMax : word;  { Max timer ticks we will wait for a brace after Esc }

    XSave, YSave : word;    { For saving / restoring cursor position }


    constructor Init;
    {- Initializes internal layer structures. Defaults transmit and receive
       buffers to the values declared above. -}


    procedure Process; virtual;
    { Buffers received data from Lower and interprets Ansi sequences IF
      they are at the head of the receive buffer. }


    procedure FindEsc;
    {- Looks for an Esc character, if followed by a brace sets Scanner state
       to CollectAnsi, if followed by another character ignores esc, if no
       following character, sets Scanner to BracketWait state. }

    procedure BracketWait;
    {- Waits for the bracket following an Esc to signal an Ansi sequence.
       If another character comes in or too many timer ticks go by, the
       Esc is ignored and processing continues. If a bracket comes in,
       the WScanner goes to the CollectAnsi state. -}

    procedure CollectAnsi;
    {- Collects characters after an Esc'[' sequence until an alpha character
       is received. Then the ansi sequence is interpreted and executed. -}



    {- These methods handle incoming data -}
    function RecvSize  : word; virtual;

    function RecvLevel : word; virtual;

    function RecvFree  : word; virtual;

    procedure RecvPeek(var Dest; var Size : word); virtual;

    procedure RecvRead(var Dest; var Size : word); virtual;

    procedure RecvWrite(var Src; var Size : word); virtual;

    procedure RecvFlush; virtual;


  end; { AnsiTerm }


implementation

const

  AnsiColorMap : array[Black..White] of byte =
    (0, 4, 2, 6, 1, 5, $3, 7, $10, $14, $12, $16, $11, $15, $13, $17);



{                         �袴袴袴袴袴袴袴袴敲
                          튦ermHost Methods �
 靈컴컴컴컴컴컴컴컴컴컴컴켕컴컴컴컴컴컴컴컴켕컴컴컴컴컴컴컴컴컴컴컴캠
 튦ermHost object method implementations.                           �
 聃컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴캭}


constructor TermHost.Init;
begin
  if not Filter.Init then Fail;
  SetLayerType(LytPresent);
  SetLayerClass('TermHost');
  TextAttr := LightGray;
end; { TermHost.Init }

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

function TermHost.TranFree : word;
var Temp : word;
begin
  Temp     := Filter.TranFree;
  TranFree := Temp - MinWord(Temp, 10);
end; { TermHost.TranFree }

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

procedure TermHost.GotoXY(X, Y : Byte);
begin TranString(Esc + '[' + DecSt(Y - 1, 0) + ';' + DecSt(X - 1, 0) + 'H') end;

procedure TermHost.ClrEol; begin TranString(Esc + '[K') end;
procedure TermHost.ClrScr; begin TranString(Esc + '[2J') end;

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

procedure TermHost.HighVideo; begin TextColor(White)    end;
procedure TermHost.LowVideo;  begin TextColor(DarkGray) end;
procedure TermHost.NormVideo; begin TextColor(LightGray)   end;

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

procedure TermHost.SendColors;
var
  AnsiSt : string[20];
  C      : byte;
  Bold   : boolean;
begin
  {- Header and blink attribute -}
  AnsiSt := Esc + '[0m' + Esc + '[';
  if (TextAttr and $80) <> 0 then AnsiSt := AnsiSt + '5;';

  {- Foreground -}
  C := AnsiColorMap[TextAttr and $0F];
  Bold := (C and $10) <> 0;
  AnsiSt := AnsiSt + DecSt((C and $F) + 30, 0) + ';';

  {- Background -}
  AnsiSt := AnsiSt + DecSt(AnsiColorMap[(TextAttr shr 4) and $07] + 40, 0);

  {- Bolding? -}
  if Bold then AnsiSt := AnsiSt + ';1m' else AnsiSt := AnsiSt + 'm';

  {- Ship it out -}
  TranString(AnsiSt);

end; { TermHost.SendColors }

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

procedure TermHost.TextBackground(Color : Byte);
begin
  TextAttr := (TextAttr and $8F) + ((Color and $07) shl 4);
  SendColors;
end; { TermHost.TextBackground }

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

procedure TermHost.TextColor(Color : Byte);
var TSt : string[10];
begin
  TextAttr := (Color and $80) +     { Blink }
              (TextAttr and $70) +  { Preserve background attributes }
              (Color and $0F);      { New color }
  SendColors;
end; { TermHost.TextColor }

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

function TermHost.KeyPressed : boolean;
begin KeyPressed := RecvLevel > 0 end;

function TermHost.ReadKey : char; begin ReadKey := RecvChar end;


{                         �袴袴袴袴袴袴袴袴敲
                          튍nsiTerm Methods �
 靈컴컴컴컴컴컴컴컴컴컴컴켕컴컴컴컴컴컴컴컴켕컴컴컴컴컴컴컴컴컴컴컴캠
 튍nsiTerm class method implementations.                            �
 聃컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴캭}

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

procedure ClrBol;
{- Clears line to beginning of line instead of end -}
var
  Ox, Oy : byte;
  S      : string[80];
begin
  Ox := WhereX; Oy := WhereY;
  GotoXY(1, Oy);
  FillChar(S[1], 80, ' ');
  S[0] := char(80);
  Write(Copy(S, 1, Ox));
  GotoXY(Ox, Oy);
end; { ClrBol }

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

procedure ClrLines(Start, Finish : word);
{- Clears a range of lines -}
var  Ox, Oy, Loop : byte;
begin
  Ox := WhereX; Oy := WhereY;
  for Loop := Start to Finish do begin
    GotoXY(1, Loop);
    ClrEol;
  end; { for }
  GotoXY(Ox, Oy);
end; { ClrLines }

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}


constructor AnsiTerm.Init;
var
  St : string[250];
  N  : word;
begin
  if not Filter.Init then Fail;
  SetLayerType(LytPresent);
  SetLayerClass('AnsiTerm');
  RHead := 1;
  RTail := 1;
  RScan := 1;
  REsc  := 0;
  EscTickMax := 3; { 1/6 of a second }
  XSave := WhereX; YSave := WhereY;
  Ptr2Method(@AnsiTerm.FindEsc, Scanner);
  TextAttr := LightGray;
end; { AnsiTerm.Init }

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

procedure AnsiTerm.Process;

  procedure AlignBufLow;
  var Shift : word;
  begin
    Shift := RTail - RHead;
    if Shift > 1 then Move(RBuf[RHead], RBuf[1], Shift);
    Dec(RHead);
    Dec(RTail, RHead);
    Dec(RScan, RHead);
    Dec(REsc, RHead);
    RHead := 1;
  end; { AlignBufLow }

  procedure FillBuf;
  var Amount : word;
  begin
    Amount := RecvFree;
    Lower^.RecvRead(RBuf[RTail], Amount);
    Inc(RTail, Amount);
  end; { FillBuf }

begin
  Filter.Process;

  {- Shift data forward in buffer if no room at end but room at front -}
  if (RTail > SizeOf(RBuf)) and (RHead > 1) then AlignBufLow;

  {- Get more data to process if there is room for it -}
  if (RTail <= SizeOf(RBuf)) and (Lower^.RecvLevel > 0) then FillBuf;

  {- Process incoming data -}
  if RScan < RTail then begin
    Waiting := false;
    repeat Scanner(@Self) until Waiting;
  end; { then }
end; { AnsiTerm.Process }

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

procedure AnsiTerm.FindEsc;
var C : char;
begin
  while (RScan < RTail) and (RBuf[RScan] <> Esc) do begin
    C := RBuf[RScan];
    if C = FF then ClrScr else Write(C);
    Inc(RScan);
  end; { while }

  {- Did we  get an ESC -}
  if (RScan < RTail) then begin
    Ptr2Method(@AnsiTerm.BracketWait, Scanner);
    EscTicks := TimerTicks;
  end { then }
  else Waiting := true;

end; { AnsiTerm.FindEsc }

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

procedure AnsiTerm.BracketWait;
begin

  {- Handle Esc characters -}
  if ((RScan + 1) < RTail) then begin
    REsc := RScan + 1;
    if RBuf[REsc] = '[' then begin
      Ptr2Method(@AnsiTerm.CollectAnsi, Scanner);
      Inc(REsc);
    end { then }
    else begin
      Ptr2Method(@AnsiTerm.FindEsc, Scanner);
      Inc(RScan, 2);
    end; { else }
  end { then }
  else
    if (TimerTicks - EscTicks) > EscTickMax then begin
      Ptr2Method(@AnsiTerm.FindEsc, Scanner);
      Inc(RScan);
    end { then }
    else Waiting := true;
end; { AnsiTerm.BracketWait }

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

procedure AnsiTerm.CollectAnsi;
var
  Ansi : string[50];
  Pl, Pc, Pn : word; { Ansi parameters }


  procedure ProcessSGR;
  const
    AnsiColors : array[0..7] of byte =
      (Black, Red, Green, Brown, Blue, Magenta, Cyan, LightGray);
  begin
    while (Length(Ansi) > 0) and (not (Ansi[1] in ['0'..'9'])) do Delete(Ansi, 1, 1);
    while Length(Ansi) > 0 do begin

      Pn := DecVal(Ansi);
      case Pn of

        {- Attributes, TextAttr format is: BbbbIfff -}
        0 : TextAttr := LightGray;         { All attributes off }
        1 : TextAttr := TextAttr or  $08;  { bold on:  or  00001000 }
        4 : TextAttr := TextAttr and $F9;  { undln on: and 11111001 mono only }
        5 : TextAttr := TextAttr or  $80;  { blink on: or  10000000 }
        7 : TextAttr := (TextAttr and $77) or $70; { blink on: or  10000000 }
        8 : TextAttr := 0;                 { Concealed on }

        {- Set colors -}
        30..37, 40..47 : begin

          {- Get color -}
          Pc := AnsiColors[Pn mod 10];

          {- Set Fore or background color sparing blink and intensity -}
          if Pn < 40 then TextAttr := (TextAttr and $F8) or Pc
          else TextBackGround(Pc);
        end; { colors }

      end; { case }
      while (Length(Ansi) > 0) and
            (Ansi[1] in ['0'..'9']) do Delete(Ansi, 1, 1);
      while (Length(Ansi) > 0) and
            (not (Ansi[1] in ['0'..'9'])) do Delete(Ansi, 1, 1);
    end; { while }

  end; { ProcessSGR }

  procedure UnKnown;
  begin
    (* Writeln('***** Unimplemented Ansi sequence "', Ansi, '" *****'); *)
  end; { UnKnown }



begin
  while (REsc < RTail) and (not (RBuf[REsc] in ['a'..'z', 'A'..'Z'])) do Inc(REsc);
  if (REsc < RTail) then begin
    {- Get sequence into a string without Esc + '[' -}
    Ansi := Mem2St(RBuf[RScan + 2], REsc - RScan - 1);

    {- Get numeric paramters -}
    case RBuf[REsc] of
      {- One numeric parm -}
      'A'..'D' : Pn := MaxWord(1, DecVal(Ansi));

      {- XY coordinates -}
      'H', 'F' : begin
        if Length(Ansi) > 1 then begin
          Pl := DecVal(Ansi);
          if Pos(';', Ansi) <> 0 then Pc := DecVal(Copy(Ansi, Pos(';',Ansi), 255))
          else Pc := 1;
        end { then }
        else begin
          Pl := 1;
          Pc := 1;
        end; { else }
      end;
    end; { case }


    {- Interpret sequence -}
    case RBuf[REsc] of

      {- CUP, HVP: Curor position -}
      'H', 'F' : GotoXY(Pc, Pl);

      {- ED: Erase in display -}
      'J' :
        case Ansi[1] of
          '2' : ClrScr; { Clear screen }

          '0', 'J' : begin  { Clear to end of screen }
            ClrEol;
            ClrLines(WhereY + 1, Hi(WindMax) + 1);
          end; { ClrEos }

          '1' : begin  { Clear to beginning of screen }
            ClrBol;
            ClrLines(1, WhereY - 1);
          end; { ClrBos}

          else UnKnown
        end; { case }

      {- EL: Erase in line -}
      'K' :
        case Ansi[1] of
          '0', 'K' : ClrEol;   { ClrEol }

          '1' : ClrBol;        { Clear from beginning of line }

          '2' : ClrLines(WhereY, WhereY);  { Clear current line }

          else UnKnown
        end; { case }

      {- SGR: Set graphics rendition -}
      'm' : ProcessSgr;

      {- CUU, CUD, CUF, CUB: Cursor up, down, forward, back -}
      'A' : GotoXY(WhereX, WhereY - Pn);
      'B' : GotoXY(WhereX, WhereY + Pn);
      'C' : GotoXY(WhereX + Pn, WhereY);
      'D' : GotoXY(WhereX - Pn, WhereY);

      {- SCP: Save cursor positiion -}
      's' : begin XSave := WhereX; YSave := WhereY end;

      {- RCP: Restore cursor positiion -}
      'u' : GotoXY(XSave, YSave);

      {- ILN: Insert line -}
      'L' : InsLine;

      {- DLN: Delete line -}
      'M' : DelLine;

      else UnKnown;
    end; { case }

    {- Delete ansi escape sequence from buffer -}
    if REsc < SizeOf(RBuf) then 
      Move(RBuf[REsc + 1], RBuf[RScan], RTail - REsc - 1);
    RTail := RScan + (RTail - REsc - 1);

    {- Return to inital scanner state -}
    Ptr2Method(@AnsiTerm.FindEsc, Scanner);
  end { then }
  else Waiting := true;
end; { AnsiTerm.CollectAnsi }

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

function AnsiTerm.RecvSize  : word;
begin RecvSize := SizeOf(RBuf) end;

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

function AnsiTerm.RecvLevel : word;
begin RecvLevel := RScan - RHead end;

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

function AnsiTerm.RecvFree : word;
begin RecvFree := SizeOf(RBuf) - RTail + 1 end;

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

procedure AnsiTerm.RecvPeek(var Dest; var Size : word);
begin
  Size := MinWord(Size, RecvLevel) ;
  if Size > 0 then Move(RBuf[RHead], Dest, Size);
end; { AnsiTerm.RecvPeek }

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

procedure AnsiTerm.RecvRead(var Dest; var Size : word);
begin
  RecvPeek(Dest, Size);
  Inc(RHead, Size);
  if RHead >= RTail then RecvFlush;
end; { AnsiTerm.RecvRead }

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

procedure AnsiTerm.RecvWrite(var Src; var Size : word);
begin
  Size := MinWord(Size, RecvFree);
  Move(Src, RBuf[RTail], Size);
  Inc(RTail, Size);
end; { AnsiTerm.RecvWrite }

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

procedure AnsiTerm.RecvFlush;
begin RHead := RScan end;

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}



end.
