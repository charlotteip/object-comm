{$I Switches.Inc }

  {靈컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
   �                           OcMux.pas                         �
   �        Copyright (C) 1990, 1991 Sycamore Microsystems       �
   �                      All rights reserved.                   �
   聃컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�}

unit OcMux;
{- Virtual circuit classes. -}

interface

uses OcLayers, OcRing, OcUtils;

{                         �袴袴袴袴袴袴袴袴敲
                          튦ABLE OF CONTENTS�
 靈컴컴컴컴컴컴컴컴컴컴컴캘                 픔컴컴컴컴컴컴컴컴컴컴컴�
 �                        �    OcMux.pas    �                       �
 �                        훤袴袴袴袴袴袴袴袴�                       �
 � Section                                         Search for       �
 � 컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴� 컴컴컴컴컴컴컴�  �
 � Definitions used by Mux/Channel classes.        (below)          �
 �                                                                  �
 � Mux class declarations.                         Mux Class        �
 �                                                                  �
 � Mux class methods.                              Mux Methods      �
 聃컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴캭}


const
  MuxChannelMax = 255;  { Maximum supported channels. }
  DefMuxMax     = 8;    { Default # of channels supported by a Mux object. }

type

  MuxPtr = ^Mux;

  {- This procedure type defines the interface from a Mux object to its
     "master". Messages are sent to the master when status changes for a
      channel. Status changes include: channel opened, channel open failed,
      channel closed/all channels closed. -}

  MuxMsgHandler = procedure(MastSelf : LayerPtr; MuxSelf : MuxPtr;
                            Msg : char; Channel : byte);

  {- MastSelf is the address of the master object, if any. This allows you to
     pass the address of a method instead of a stand-alone procedure. MastSelf
     can be nil if you are using a stand-alone procedure to handle a mux.

     MuxSelf is the address of the Mux object sending the message. Msg is the
     message character. Channel is the affected Mux channel, if any.

     Here is a table of all valid message codes and their meanings:

     Msg Desc
     --- ----------------------------------------------------------------------
     NUL No message, ignore or do periodic processing. Channel ignored.

     ENQ Channel opened by other end.

     ACK Channel open request acknowledged.

     Note: After Mux notifies your message handler about a new channel, via
           an ENQ or ACK message, the channel is your application's
           responsibility. Your app. can get a pointer to the channel's
           RingBuf object using the ChannelPtr method (below). Then it can
           set RingOptions and call the Activate method to enable the channel.
           If Activate fails or the application wants to close the channel for
           another reason, it should call the CloseChannel method.

     NAK Channel open request rejected, attempt to open new channel failed.
         The Channel entry will be destroyed after the message handler returns.

     CAN Channel will be destroyed after message handler returns. Remove any
         incoming data still in the receive buffer and destroy or deactivate
         any layer object using the channel. If Channel is 0, then all channels
         will be destroyed and the Mux object itself will Deativate, after the
         message handler returns. -}


  {- Mux protocol format.

     There are 5 message types:

     Enquiry, requests a new channel with these parameters. Receiver must ACK or
       NAK an enquiry.

       <ENQ>,       1
       <Channel>,   1
       <Priority>,  1
       <MaxMsgSz>,  2
       <Userdata>,  4
       --------------
                    9

     Acknowledge, positive resonse to an ENQ. Channel may now be used.

       <ACK>,       1
       <Channel>,   1
       --------------
                    2

     Not Acknowledge, negative resonse to an ENQ. Channel no good, if Avail is
       not 0, the ENQ may be retried with that channel.

       <NAK>,       1
       <Channel>,   1
       <Avail>,     1
       --------------
                    3

     Cancel, closes the channel. If channel is 0, all open channels are closed and
       the Mux session is terminated.

       <CAN>,       1
       <Channel>,   1
       --------------
                    2

     Data, transmit channel data.

       <STX>,       1
       <Channel>,   1
       <Length>,    2
       <Data>,      variable.
       --------------
                    5+      -}


  {- One of these per channel. -}
  ChanRec = record
    ID       : byte;       { Channel # }
    Prior    : byte;       { Channel priority, higher is more. }
    MaxMsg   : word;       { Maximum message size for channel. }
    Key      : longint;    { User defined ID for channel, sent with Init. }
    ChLyrPtr : RingBufPtr; { Ring buffer object for this channel. }
  end; { ChanRec }
  ChanAraPtr = ^ChanAra;
  ChanAra    = array[1..MuxChannelMax] of ChanRec;

  {- Mux receive scanner states -}
  MuxRcStates = (MsgType, MsgBody, MsgData);


{                         �袴袴袴袴袴袴袴袴敲
                          �    Mux Class    �
 靈컴컴컴컴컴컴컴컴컴컴컴켕컴컴컴컴컴컴컴컴켕컴컴컴컴컴컴컴컴컴컴컴캠
 튝erges and unmerges several logical data streams into one physical�
 �  data stream.                                                    �
 聃컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴캭}

  {- The Mux class -}
  Mux = object(Layer)
    ChSize   : byte;       { Number of channels allocated. }
    ChUsed   : byte;       { Number of channels open. }
    Chnls    : ChanAraPtr; { Our channels. }

    MuxMsgs  : MuxMsgHandler; { Pointer to the Mux message handler }
    MastPtr  : LayerPtr;      { Pointer to master of this Mux. }

    {- Receiver -}
    RcState  : MuxRcStates;   { State of receive scanner: 1..3. }
    RcMsg    : char;          { Type of message we got. }
    RcCh     : byte;          { Current receive channel. }
    RcBuf    : array[1..10] of byte;  { Stores any extra bytes coming in. -}
    RcNeed   : word;          { Bytes needed to finish current message. }
    RcLayer  : RingBufPtr;    { Pointer to RingBuf for current channel. }


    constructor Init(MasterLayer : LayerPtr; MsgHandler : pointer);
    {- Constructs Mux assigns message handler and master layer pointers. -}

    procedure MuxOptions(Channels : byte);
    {- Sets maximum number of channels we will use. -}

    function Activate : boolean; virtual;
    {- Allocates channel table from heap. Tries to find other end. -}

    procedure Deactivate; virtual;
    {- DeActivates mux layer. -}

    procedure Process; virtual;
    {- Trys to receive and transmit data for upper ring buffers. -}


    {- Mux specific methods -}

    function OpenChannel(Priority : byte; MaxMsgSize : word;
                         UserKey : longint) : byte;
    {- Initiates channel open sequence. Returns 0 if channel succesfully
       initiated. Channel cannot be used until other end confirms with an
       ACK message. Then the MuxMsgHandler will be called with the opened
       channel's number. It is up to the calling layer to compare UserData
       values and figure out which channel is which if multiple channels are
       being opened at the same time. If channels are to be unique, then the
       application should make sure that all UserData values are unique.

       Other possible OpenChannel errors are:

         1 : no vacant channels left.
         2 : no room in transmitter to send ENQ message.
         3 : AddCh method call failed, probably out of heap.
        -}

    procedure CloseChannel(Channel : byte);
    {- Closes and destroys a Mux Channel -}

    function SeekChannelKey(UserKey : longint) : byte;
    {- Returns channel number of first channel with a certain key, or 0 if
       the key was not found. -}

    function ChannelsLeft : byte;
    {- Vacant channel slots available. -}

    function ChannelExist(Channel : byte) : boolean;

    function ChannelPtr(Channel : byte) : RingBufPtr;

    function ChannelKey(Channel : byte) : longint;


    {- Internals -}

    procedure Receiver;
    {- Called by Process method to handle incoming data stream. -}

    procedure Transmitter;
    {- Called by Process method to handle outgoing data stream. -}

    function SeekCh(Channel : byte) : word;
    {- Returns channel array index for channel # Channel, 0 for not found. -}

    function AddCh(Channel : byte; Priority : byte; MaxSize : word;
                   UserKey : longint) : boolean;
    {- Adds a channel to the channel table. Returns true if succesful.
       If already there, just returns true. -}

    procedure DelCh(Channel : byte);
    {- Deletes a channel from the channel table. -}

    function FindUnusedCh : byte;
    {- Returns unused channel number. -}

    function MsgLen(Msg : char) : byte;
    {- Returns # of bytes to receive following the Message ID and chan ID. -}

  end; { Mux }




implementation



{                         �袴袴袴袴袴袴袴袴敲
                          �   Mux Methods   �
 靈컴컴컴컴컴컴컴컴컴컴컴켕컴컴컴컴컴컴컴컴켕컴컴컴컴컴컴컴컴컴컴컴캠
 튝ux class methods implementations.                                �
 聃컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴캭}

constructor Mux.Init(MasterLayer : LayerPtr; MsgHandler : pointer);
begin
  {- Init fields -}
  if (not Layer.Init) or (MsgHandler = nil) then Fail;

  SetLayerType(LytSession);
  SetLayerClass('Mux');

  ChSize   := DefMuxMax;
  Chnls    := nil;

  MastPtr  := MasterLayer;
  Move(MsgHandler, MuxMsgs, 4);
end; { Mux.Init }

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

procedure Mux.MuxOptions(Channels : byte);
begin ChSize := MaxWord(1, MinWord(Channels, MuxChannelMax)) end;

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

function Mux.Activate : boolean;
begin
  Activate := false;
  if not Layer.Activate then Exit;

  {- See if there is a lower layer. -}
  if Lower = nil then begin
    SetLayerError(LyeNoLower);
    Exit;
  end; { then }

  {- See if lower layer is too small. -}
  if Lower^.RecvSize < 50 then begin
    SetLayerError(LyeBufTooSmall);
    Exit;
  end; { then }

  {- Allocate buffers from the heap -}
  if Chnls = nil then begin
    GetMem(Chnls, ChSize * SizeOf(ChanRec));
    if Chnls = nil then begin
      SetLayerError(LyeOutOfHeap);
      Exit;
    end; { then }
    ChUsed := 0;
  end; { then }

  {- Init receiver -}
  RcState := MsgType;
  RcNeed  := 2;
  Activate := true;
end; { Mux.Activate }

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

procedure Mux.Deactivate;
begin

  {- Wipe out Chnls -}
  if ChUsed > 0 then begin
    Lower^.TranString(CAN + #0);
    Lower^.Process;
    MuxMsgs(MastPtr, @Self, CAN, 0);
    while ChUsed > 0 do DelCh(Chnls^[ChUsed].Id);
    FreeMem(Chnls, ChSize * SizeOf(ChanRec));
    Chnls := nil;
  end; { then }
  Layer.Deactivate;
end; { Mux.Deactivate }

{- Process method and related routines.- - - - - - - - - - - - - - - - - - - -}

procedure Mux.Process;
begin
  if LayerStatus = LysInactive then Exit;
  Layer.Process;
  Receiver;
  Transmitter;
  Layer.Process;
end; { Mux.Process }

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

procedure Mux.Receiver;
{- Processes incoming messages and data. -}
var
  OutB   : array[1..10] of byte; { Buffer for output messages. }
  LoRLvl : word;                 { Value of Lower^.RecvLevel, cuts overhead. }
  Waiting : boolean;             { True when waiting on something. }

  {. . . . . . . . . . . . .}

  procedure MuxDataDist;
  {- Processes and distributes incoming data. -}
  const CopyBufSize = 250;
  var
    Chunk,         { Total number of bytes to move this time. -}
    Piece : word;  { # of bytes per copy buffer sequence. -}
    CopyBuf : array[1..CopyBufSize] of byte;
  begin
    {- Move data -}
    if RcLayer = nil then Exit;
    Chunk := MinWord(MinWord(RcNeed, LoRLvl), RcLayer^.RecvFree);
    if Chunk = 0 then begin
      Waiting := true;
      Exit;
    end { then }
    else begin
      {- Move chunk of data from below to upper in pieces. -}
      Dec(RcNeed, Chunk);
      while Chunk > 0 do begin
        Piece := MinWord(Chunk, CopyBufSize);
        Lower^.RecvRead(CopyBuf, Piece);
        RcLayer^.RecvWrite(CopyBuf, Piece);
        Dec(Chunk, Piece);
      end; { while }
    end; { else }

    {- Change state if we finished this message. -}
    if RcNeed = 0 then begin
      RcState := MsgType;
      RcNeed  := 2;
    end; { then }
  end; { MuxDataDist }

  {. . . . . . . . . . . . .}

  procedure RecvENQ;
  {- Processes ENQ messages. -}
  var
    MaxSz : word;
    UserD : longint;
  begin
    {- Be pessimistic and assume a NAK reply. -}
    OutB[1] := byte(NAK);
    OutB[2] := RcCh;
    RcNeed  := 3;

    {- See if channel table is full already. -}
    if ChUsed >= ChSize then OutB[3] := 0
    else
      {- See if the channel already exists -}
      if SeekCh(RcCh) <> 0 then OutB[3] := FindUnusedCh
      else begin

        {- See if we can add the channel -}
        Move(RcBuf[2], MaxSz, 2);
        Move(RcBuf[4], UserD, 4);
        if AddCh(RcCh, RcBuf[1], MaxSz, UserD) then begin
          OutB[1] := byte(ACK);
          Dec(RcNeed);
          MuxMsgs(MastPtr, @Self, ENQ, RcCh);
        end
        else OutB[3] := 0;
      end; { then }

    {- Transmit ACK or NAK -}
    Lower^.TranWrite(OutB, RcNeed);
  end; { RecvENQ }

  {. . . . . . . . . . . . .}

begin { Mux.Receiver }
  Waiting := false;
  LoRLvl := Lower^.RecvLevel;
  while ((LoRLvl >= RcNeed) or ((RcState = MsgData) and (Lower^.RecvFree = 0)))
        and (not Waiting) and (Lower^.TranFree >= 10) do begin
    {- Read data -}
    case RcState of

      {- Distribute incoming data. -}
      MsgData : if RcLayer^.LayerStatus > LysInActive then MuxDataDist;

      {- Get message type character. -}
      MsgType : begin
        RcMsg   := Lower^.RecvChar;
        RcCh    := byte(Lower^.RecvChar);
        RcNeed  := MsgLen(RcMsg);
        RcState := MsgBody;
      end; { MsgType }

      {- Process different types of messages. -}
      MsgBody : begin

        {- Get body of message -}
        if RcNeed > 0 then Lower^.RecvRead(RcBuf, RcNeed);

        {- Handle Data and other messages -}
        if RcMsg = STX then begin
          Move(RcBuf, RcNeed, 2);
          RcState := MsgData;
          if SeekCh(RcCH) = 0 then RcLayer := nil
          else RcLayer := Chnls^[SeekCh(RcCh)].ChLyrPtr;
        end { then }
        else begin

          {- Sort out non-data messages -}
          case RcMsg of
            {- Open new channel inquiry -}
            ENQ : RecvENQ;

            {- Channel open was OK -}
            ACK : MuxMsgs(MastPtr, @Self, ACK, RcCh);

            {- Channel open rejected -}
            NAK : begin
              {- Can we retry? -}
              if RcBuf[1] = 0 then begin
                MuxMsgs(MastPtr, @Self, NAK, RcCh);
                DelCh(RcCh);
              end { then }
              else begin
                {- Send ENQ with another channel -}
                {- First, see if their recommended channel conflicts... -}
                if SeekCh(RcBuf[1]) <> 0 then RcBuf[1] := FindUnusedCh;

                {- Send new ENQ -}
                with Chnls^[SeekCh(RcCh)] do begin
                  OutB[1] := byte(ENQ);
                  ID      := RcBuf[1];
                  OutB[2] := ID;
                  OutB[3] := Prior;
                  Move(MaxMsg, OutB[4], 2);
                  Move(Key, OutB[6], 4);
                  RcNeed  := 9;
                  Lower^.TranWrite(OutB, RcNeed);
                end; { with }
              end; { else }
            end; { NAK }

            {- Channel cancel -}
            CAN : begin
              {- Cancel one channel or all? -}
              MuxMsgs(MastPtr, @Self, CAN, RcCh);
              if RcCh <> 0 then DelCh(RcCh) else Deactivate;
            end; { CAN }
          end; { case }

          {- Reset state -}
          RcNeed  := 1;
          RcState := MsgType;
        end; { else }
      end; { MsgBody }

    end; { case }

    {- Update level keeper. -}
    LoRLvl := Lower^.RecvLevel;
  end; { while }
end; { Mux.Receiver }

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

procedure Mux.Transmitter;
const CopyBufSize = 250;
var
  CopyBuf : array[1..CopyBufSize] of byte;
  LwrTFree : word;    { Current lower TranFree - 20 }
  Waiting : boolean;  { True if transmitter waiting  }
  Idle    : boolean;  { True if no channels are active }
  TLoop   : word;
  Chunk   : word;    { # of data bytes we will transmit from one channel. }
  Piece   : word;    { # of bytes moved per piece. -}
begin
  if ChUsed = 0 then Exit;

  {- Loop until nothing can be transmitted -}
  LwrTFree := Lower^.TranFree;
  if LwrTFree < 20 then LwrTFree := 0;
  Idle := false;
  while not Idle do begin

    {- Scan the active channels trying to transmit -}
    Idle  := true;
    TLoop := 1;
    while (TLoop <= ChUsed) and (LwrTFree > 0) do begin

      {- Channel, layer pointer -}
      with Chnls^[TLoop] do
        with ChLyrPtr^ do begin

          {- Is this layer active yet? -}
          if LayerStatus > LysInActive then begin

            {- Any data to transmit here? -}
            Chunk := MinWord(MaxMsg, MinWord(TranLevel, LwrTFree));
            if Chunk > 0 then begin

              {- Send header -}
              Idle := false;
              CopyBuf[1] := byte(STX);
              CopyBuf[2] := Id;
              Move(Chunk, CopyBuf[3], 2);
              Piece := 4;
              Self.Lower^.TranWrite(CopyBuf, Piece);

              {- Send data -}
              while Chunk > 0 do begin
                Piece := MinWord(Chunk, CopyBufSize);
                TranRead(CopyBuf, Piece);
                Self.Lower^.TranWrite(CopyBuf, Piece);
                Dec(Chunk, Piece);
              end; { while }
            end; { then }
          end; { then }
        end; { with }

      {- Update loop vars -}
      Inc(TLoop);
      LwrTFree := Lower^.TranFree;
      if LwrTFree < 20 then LwrTFree := 0;
    end; { while }
  end; { while }

end; { Mux.Transmitter }

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

function Mux.OpenChannel(Priority : byte; MaxMsgSize : word;
                         UserKey : longint) : byte;
var
  OutB : array[1..10] of byte;
  TSize : word;
begin
  {- Get free channel, if any. -}
  OutB[2] := FindUnusedCh;
  if OutB[2] = 0 then begin
    OpenChannel := 1;
    Exit;
  end; { then }

  {- Enough room to send ENQ message? -}
  if Lower^.TranFree < 10 then begin
    OpenChannel := 2;
    Exit;
  end; { then }

  {- Try to create the channel record -}
  if not AddCh(OutB[2], Priority, MaxWord(1, MaxMsgSize), UserKey) then begin
    OpenChannel := 3;
    Exit;
  end; { then }

  {- Send ENQ message -}
  OutB[1] := byte(ENQ);
  { OutB[2] set above to new channel ID. }
  OutB[3] := Priority;
  Move(MaxMsgSize, OutB[4], 2);
  Move(UserKey, OutB[6], 4);
  TSize := 9;
  Lower^.TranWrite(OutB, TSize);
  OpenChannel := 0;
end; { Mux.OpenChannel }

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

procedure Mux.CloseChannel(Channel : byte);
begin
  if Channel = 0 then Deactivate
  else begin
    Lower^.TranString(CAN + char(Channel));
    DelCh(Channel);
  end; { else }
end; { Mux.CloseChannel }

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

function Mux.SeekChannelKey(UserKey : longint) : byte;
var SLoop : word;
begin
  SLoop  := 1;
  while (SLoop <= ChUsed) and (Chnls^[SLoop].Key <> UserKey) do Inc(SLoop);
  if SLoop <= ChUsed then SeekChannelKey := SLoop else SeekChannelKey := 0;
end; { Mux.SeekChannelKey }

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

function Mux.ChannelsLeft : byte;
begin ChannelsLeft := ChSize - ChUsed end;

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

function Mux.ChannelExist(Channel : byte) : boolean;
begin ChannelExist := SeekCh(Channel) <> 0 end;

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

function Mux.ChannelPtr(Channel : byte) : RingBufPtr;
var ChanIdx : word;
begin
  ChanIdx := SeekCh(Channel);
  if ChanIdx = 0 then ChannelPtr := nil else ChannelPtr := Chnls^[ChanIdx].ChLyrPtr;
end; { Mux.ChannelPtr }

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

function Mux.ChannelKey(Channel : byte) : longint;
var ChanIdx : word;
begin
  ChanIdx := SeekCh(Channel);
  if ChanIdx = 0 then ChannelKey := 0 else ChannelKey := Chnls^[ChanIdx].Key;
end; { Mux.ChannelKey }

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

function Mux.SeekCh(Channel : byte) : word;
var SLoop : word;
begin
  SLoop  := 1;
  while (SLoop <= ChUsed) and (Chnls^[SLoop].Id <> Channel) do Inc(SLoop);
  if SLoop <= ChUsed then SeekCh := SLoop else SeekCh := 0;
end; { Mux.SeekCh }

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

function Mux.AddCh(Channel : byte; Priority : byte; MaxSize : word;
               UserKey : longint) : boolean;
var
  ILoop : word;
  TRing : RingBufPtr;
begin
  AddCh := false;

  {- Any slots left? -}
  AddCh := false;
  if ChUsed = ChSize then Exit;

  {- Is this channel already in use? -}
  if SeekCh(Channel) <> 0 then Exit;

  {- Allocate the RingBuf pointer, if possible. -}
  TRing := New(RingBufPtr, Init);
  if TRing = nil then Exit;
  TRing^.Lower := @Self;     {MOD 5-10-91}

  {- Locate insertion point. -}
  ILoop := 1;
  while (ILoop <= ChUsed) and (Priority <= Chnls^[ILoop].Prior) do Inc(ILoop);

  {- Shift other channels if we are inserting. -}
  if ILoop <= ChUsed then
    Move(Chnls^[ILoop],
         Chnls^[ILoop + 1], (ChUsed - ILoop + 1) * SizeOf(ChanRec));

  {- Fill in new data -}
  with Chnls^[ILoop] do begin
    Id       := Channel;
    Prior    := Priority;
    MaxMsg   := MaxSize;
    Key      := UserKey;
    ChLyrPtr := TRing;
  end; { then }

  Inc(ChUsed);
  AddCh := true;
end; { Mux.AddCh }

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

procedure Mux.DelCh(Channel : byte);
var DLoc : word;
begin
  {- Is this channel in use? -}
  DLoc := SeekCh(Channel);
  if DLoc = 0 then Exit;

  {- Destroy the RingBuf for this channel. -}
  Dispose(Chnls^[DLoc].ChLyrPtr, Done);

  {- Shift other channels if we are deleting from middle of array. -}
  if DLoc < ChUsed then
    Move(Chnls^[DLoc + 1], Chnls^[DLoc], (ChUsed - DLoc) * SizeOf(ChanRec));
  Dec(ChUsed);
end; { Mux.DelCh }

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

function Mux.FindUnusedCh : byte;
var
  TimerLo : byte absolute TimerTicks;
  Start,
  SLoop,
  Found : byte;
begin
  Start := TimerLo;
  SLoop := Start + 1;
  Found := 1;

  while (Found <> 0) and (SLoop <> Start) do begin
    if SLoop <> 0 then Found := SeekCh(SLoop);
    Inc(Sloop);
  end; { while }
  Dec(SLoop);
  if Found = 0 then FindUnusedCh := SLoop else FindUnusedCh := 0;
end; { Mux.FindUnusedCh }

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

function Mux.MsgLen(Msg : char) : byte;
begin
  case Msg of
    STX  : MsgLen := 2;
    ENQ  : MsgLen := 7;
    NAK  : MsgLen := 1;
  else     MsgLen := 0;
  end; { case }
end; { Mux.MsgLen }

{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -}

begin end.
