;  靈컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
;  �                          OcUtils.asm                        �
;  �           Copyright (C) 1991, Sycamore Microsystems         �
;  �                      All rights reserved.                   �
;  聃컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�}


;                         �袴袴袴袴袴袴袴袴敲
;                         튦ABLE OF CONTENTS�
;靈컴컴컴컴컴컴컴컴컴컴컴캘                 픔컴컴컴컴컴컴컴컴컴컴컴�
;�                        �   OcUtils.asm   �                       �
;�                        훤袴袴袴袴袴袴袴袴�                       �
;� Section                                         Search for       �
;� 컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴� 컴컴컴컴컴컴컴�  �
;� CRC-16 shift values table.                      (below)          �
;� CRC-32 shift values.                            CRC32Table       �
;� Function to update a 16-bit CRC.                CRC16Update      �
;� Function to update a 32-bit CRC.                CRC32Update      �
;聃컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴캭

	IDEAL
	MODEL	TPASCAL

	CODESEG
	PUBLIC	Crc16Table,  Crc32Table
	PUBLIC	Crc16UpDate, Crc32Update


;                         �袴袴袴袴袴袴袴袴敲
;                         �   Crc16Table    �
;靈컴컴컴컴컴컴컴컴컴컴컴켕컴컴컴컴컴컴컴컴켕컴컴컴컴컴컴컴컴컴컴컴캠
;� Table of shift values for computing CRC-16, calculated by Mark G.� 
;� Mendel, Network Systems Corporation.                             �
;聃컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴캭


CRC16Table dw 00000h

 dw         01021h, 02042h, 03063h, 04084h, 050a5h, 060c6h, 070e7h, 08108h, 09129h
 dw 0a14ah, 0b16bh, 0c18ch, 0d1adh, 0e1ceh, 0f1efh, 01231h, 00210h, 03273h, 02252h
 dw 052b5h, 04294h, 072f7h, 062d6h, 09339h, 08318h, 0b37bh, 0a35ah, 0d3bdh, 0c39ch
 dw 0f3ffh, 0e3deh, 02462h, 03443h, 00420h, 01401h, 064e6h, 074c7h, 044a4h, 05485h
 dw 0a56ah, 0b54bh, 08528h, 09509h, 0e5eeh, 0f5cfh, 0c5ach, 0d58dh, 03653h, 02672h
 dw 01611h, 00630h, 076d7h, 066f6h, 05695h, 046b4h, 0b75bh, 0a77ah, 09719h, 08738h
 dw 0f7dfh, 0e7feh, 0d79dh, 0c7bch, 048c4h, 058e5h, 06886h, 078a7h, 00840h, 01861h
 dw 02802h, 03823h, 0c9cch, 0d9edh, 0e98eh, 0f9afh, 08948h, 09969h, 0a90ah, 0b92bh
 dw 05af5h, 04ad4h, 07ab7h, 06a96h, 01a71h, 00a50h, 03a33h, 02a12h, 0dbfdh, 0cbdch
 dw 0fbbfh, 0eb9eh, 09b79h, 08b58h, 0bb3bh, 0ab1ah, 06ca6h, 07c87h, 04ce4h, 05cc5h
 dw 02c22h, 03c03h, 00c60h, 01c41h, 0edaeh, 0fd8fh, 0cdech, 0ddcdh, 0ad2ah, 0bd0bh
 dw 08d68h, 09d49h, 07e97h, 06eb6h, 05ed5h, 04ef4h, 03e13h, 02e32h, 01e51h, 00e70h
 dw 0ff9fh, 0efbeh, 0dfddh, 0cffch, 0bf1bh, 0af3ah, 09f59h, 08f78h, 09188h, 081a9h
 dw 0b1cah, 0a1ebh, 0d10ch, 0c12dh, 0f14eh, 0e16fh, 01080h, 000a1h, 030c2h, 020e3h
 dw 05004h, 04025h, 07046h, 06067h, 083b9h, 09398h, 0a3fbh, 0b3dah, 0c33dh, 0d31ch
 dw 0e37fh, 0f35eh, 002b1h, 01290h, 022f3h, 032d2h, 04235h, 05214h, 06277h, 07256h
 dw 0b5eah, 0a5cbh, 095a8h, 08589h, 0f56eh, 0e54fh, 0d52ch, 0c50dh, 034e2h, 024c3h
 dw 014a0h, 00481h, 07466h, 06447h, 05424h, 04405h, 0a7dbh, 0b7fah, 08799h, 097b8h
 dw 0e75fh, 0f77eh, 0c71dh, 0d73ch, 026d3h, 036f2h, 00691h, 016b0h, 06657h, 07676h
 dw 04615h, 05634h, 0d94ch, 0c96dh, 0f90eh, 0e92fh, 099c8h, 089e9h, 0b98ah, 0a9abh
 dw 05844h, 04865h, 07806h, 06827h, 018c0h, 008e1h, 03882h, 028a3h, 0cb7dh, 0db5ch
 dw 0eb3fh, 0fb1eh, 08bf9h, 09bd8h, 0abbbh, 0bb9ah, 04a75h, 05a54h, 06a37h, 07a16h
 dw 00af1h, 01ad0h, 02ab3h, 03a92h, 0fd2eh, 0ed0fh, 0dd6ch, 0cd4dh, 0bdaah, 0ad8bh
 dw 09de8h, 08dc9h, 07c26h, 06c07h, 05c64h, 04c45h, 03ca2h, 02c83h, 01ce0h, 00cc1h
 dw 0ef1fh, 0ff3eh, 0cf5dh, 0df7ch, 0af9bh, 0bfbah, 08fd9h, 09ff8h, 06e17h, 07e36h
 dw 04e55h, 05e74h, 02e93h, 03eb2h, 00ed1h, 01ef0h



;                         �袴袴袴袴袴袴袴袴敲
;                         �   Crc32Table    �
;靈컴컴컴컴컴컴컴컴컴컴컴켕컴컴컴컴컴컴컴컴켕컴컴컴컴컴컴컴컴컴컴컴캠
;튦able of shift values for computing CRC-32.                       �
;�                                                                  �
;� Copyright (C) 1986 Gary S. Brown.  You may use this program, or  �
;� code or tables extracted from it, as desired without restriction.�
;聃컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴캭

CRC32Table   dd 000000000h, 077073096h, 0ee0e612ch, 0990951bah, 0076dc419h
             dd 0706af48fh, 0e963a535h, 09e6495a3h, 00edb8832h, 079dcb8a4h
             dd 0e0d5e91eh, 097d2d988h, 009b64c2bh, 07eb17cbdh, 0e7b82d07h
             dd 090bf1d91h, 01db71064h, 06ab020f2h, 0f3b97148h, 084be41deh
             dd 01adad47dh, 06ddde4ebh, 0f4d4b551h, 083d385c7h, 0136c9856h
             dd 0646ba8c0h, 0fd62f97ah, 08a65c9ech, 014015c4fh, 063066cd9h
             dd 0fa0f3d63h, 08d080df5h, 03b6e20c8h, 04c69105eh, 0d56041e4h
             dd 0a2677172h, 03c03e4d1h, 04b04d447h, 0d20d85fdh, 0a50ab56bh
             dd 035b5a8fah, 042b2986ch, 0dbbbc9d6h, 0acbcf940h, 032d86ce3h
             dd 045df5c75h, 0dcd60dcfh, 0abd13d59h, 026d930ach, 051de003ah
             dd 0c8d75180h, 0bfd06116h, 021b4f4b5h, 056b3c423h, 0cfba9599h
             dd 0b8bda50fh, 02802b89eh, 05f058808h, 0c60cd9b2h, 0b10be924h
             dd 02f6f7c87h, 058684c11h, 0c1611dabh, 0b6662d3dh, 076dc4190h
             dd 001db7106h, 098d220bch, 0efd5102ah, 071b18589h, 006b6b51fh
             dd 09fbfe4a5h, 0e8b8d433h, 07807c9a2h, 00f00f934h, 09609a88eh
             dd 0e10e9818h, 07f6a0dbbh, 0086d3d2dh, 091646c97h, 0e6635c01h
             dd 06b6b51f4h, 01c6c6162h, 0856530d8h, 0f262004eh, 06c0695edh
             dd 01b01a57bh, 08208f4c1h, 0f50fc457h, 065b0d9c6h, 012b7e950h
             dd 08bbeb8eah, 0fcb9887ch, 062dd1ddfh, 015da2d49h, 08cd37cf3h
             dd 0fbd44c65h, 04db26158h, 03ab551ceh, 0a3bc0074h, 0d4bb30e2h
             dd 04adfa541h, 03dd895d7h, 0a4d1c46dh, 0d3d6f4fbh, 04369e96ah
             dd 0346ed9fch, 0ad678846h, 0da60b8d0h, 044042d73h, 033031de5h
             dd 0aa0a4c5fh, 0dd0d7cc9h, 05005713ch, 0270241aah, 0be0b1010h
             dd 0c90c2086h, 05768b525h, 0206f85b3h, 0b966d409h, 0ce61e49fh
             dd 05edef90eh, 029d9c998h, 0b0d09822h, 0c7d7a8b4h, 059b33d17h
             dd 02eb40d81h, 0b7bd5c3bh, 0c0ba6cadh, 0edb88320h, 09abfb3b6h
             dd 003b6e20ch, 074b1d29ah, 0ead54739h, 09dd277afh, 004db2615h
             dd 073dc1683h, 0e3630b12h, 094643b84h, 00d6d6a3eh, 07a6a5aa8h
             dd 0e40ecf0bh, 09309ff9dh, 00a00ae27h, 07d079eb1h, 0f00f9344h
             dd 08708a3d2h, 01e01f268h, 06906c2feh, 0f762575dh, 0806567cbh
             dd 0196c3671h, 06e6b06e7h, 0fed41b76h, 089d32be0h, 010da7a5ah
             dd 067dd4acch, 0f9b9df6fh, 08ebeeff9h, 017b7be43h, 060b08ed5h
             dd 0d6d6a3e8h, 0a1d1937eh, 038d8c2c4h, 04fdff252h, 0d1bb67f1h
             dd 0a6bc5767h, 03fb506ddh, 048b2364bh, 0d80d2bdah, 0af0a1b4ch
             dd 036034af6h, 041047a60h, 0df60efc3h, 0a867df55h, 0316e8eefh
             dd 04669be79h, 0cb61b38ch, 0bc66831ah, 0256fd2a0h, 05268e236h
             dd 0cc0c7795h, 0bb0b4703h, 0220216b9h, 05505262fh, 0c5ba3bbeh
             dd 0b2bd0b28h, 02bb45a92h, 05cb36a04h, 0c2d7ffa7h, 0b5d0cf31h
             dd 02cd99e8bh, 05bdeae1dh, 09b64c2b0h, 0ec63f226h, 0756aa39ch
             dd 0026d930ah, 09c0906a9h, 0eb0e363fh, 072076785h, 005005713h
             dd 095bf4a82h, 0e2b87a14h, 07bb12baeh, 00cb61b38h, 092d28e9bh
             dd 0e5d5be0dh, 07cdcefb7h, 00bdbdf21h, 086d3d2d4h, 0f1d4e242h
             dd 068ddb3f8h, 01fda836eh, 081be16cdh, 0f6b9265bh, 06fb077e1h
             dd 018b74777h, 088085ae6h, 0ff0f6a70h, 066063bcah, 011010b5ch
             dd 08f659effh, 0f862ae69h, 0616bffd3h, 0166ccf45h, 0a00ae278h
             dd 0d70dd2eeh, 04e048354h, 03903b3c2h, 0a7672661h, 0d06016f7h
             dd 04969474dh, 03e6e77dbh, 0aed16a4ah, 0d9d65adch, 040df0b66h
             dd 037d83bf0h, 0a9bcae53h, 0debb9ec5h, 047b2cf7fh, 030b5ffe9h
             dd 0bdbdf21ch, 0cabac28ah, 053b39330h, 024b4a3a6h, 0bad03605h
             dd 0cdd70693h, 054de5729h, 023d967bfh, 0b3667a2eh, 0c4614ab8h
             dd 05d681b02h, 02a6f2b94h, 0b40bbe37h, 0c30c8ea1h, 05a05df1bh
             dd 02d02ef8dh



;                         �袴袴袴袴袴袴袴袴敲
;                         �   Crc16Update   �
;靈컴컴컴컴컴컴컴컴컴컴컴켕컴컴컴컴컴컴컴컴켕컴컴컴컴컴컴컴컴컴컴컴캠
;튧pdates a 16-bit CRC code. Links to Pascal.                       �
;聃컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴캭
; Usage:
;
;  AX is the work register.
;  BX calculates index into Crc16Table, does work.
;  CX is the loop register.
;  DX holds the Crc.
;  ES:DI addresses data to calculate Crc for.
;
;
;======================================================================
PROC 	Crc16Update FAR
	ARG OldCrc : WORD, DataBytes : DWORD, DataSize : WORD

		; Load parameters, set up registers.
		les	di, [DataBytes]		; pointer to data.
		mov	cx, [DataSize]		; Size of data.
		mov	dx, [OldCrc]		; Load current Crc.
	
	; The Crc loop
	@@CrcLoop:
	
		; Crc16Table[Hi(Crc) xor Data[Loop]] ...
		mov	bl, dh		; Hi(Crc)
		xor	bl, [es:di]	; Data[Loop]
		inc	di		; advance source pointer
		xor	bh, bh
		shl	bx, 1		; Address words
		mov	bx, [Crc16Table + bx]
	
		; ... xor (Lo(Crc) SHL 8)
		mov	ah, dl
		xor	al, al
		xor	bx, ax
		mov	dx, bx
		
	loop @@CrcLoop

		mov	ax, dx
		ret
ENDP	Crc16Update



;                         �袴袴袴袴袴袴袴袴敲
;                         �   Crc32Update   �
;靈컴컴컴컴컴컴컴컴컴컴컴켕컴컴컴컴컴컴컴컴켕컴컴컴컴컴컴컴컴컴컴컴캠
;튧pdates a 32-bit CRC code. Links to Pascal.                       �
;聃컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴캭
; This routine computes the 32 bit CRC used by PKZIP and its derivatives,
; and by Chuck Forsberg's "ZMODEM" protocol.  The block CRC computation
; should start with high-values (0ffffffffh), and finish by inverting all
; bits.
;
; Most of this TASM conversion done by: Edwin T. Floyd (Thanks)
; Uses registers: AX, BX, CX, DX, ES, SI, and FLAGS
;======================================================================
PROC 	Crc32Update FAR
	ARG OldCrc : DWORD, DataBytes : DWORD, DataSize : WORD

; CRC32Update takes an initial CRC value and updates it with DataSize bytes from
; DataBytes. The updated CRC is returned in DX:AX.
;

         push   ds
         lds    si,[DataBytes]  ; buffer to process
         les    ax,[OldCrc]	; Current CRC value
         mov    dx,es
         mov    cx,[DataSize]	; # of bytes to process
         or     cx,cx
         jz     @@done
         cld			; Added by JRT, May 1991.
         
@@loop:
         xor    bh,bh
         mov    bl,al
         lodsb
         xor    bl,al
         mov    al,ah
         mov    ah,dl
         mov    dl,dh
         xor    dh,dh
         shl    bx,1
         shl    bx,1
         les    bx,[Crc32Table + bx]
         xor    ax,bx
         mov    bx,es
         xor    dx,bx
         loop   @@loop
@@done:
         pop    ds
         ret
ENDP	Crc32Update

END

