;  靈컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
;  �                          OcEquInc.asm                       �
;  �           Copyright (C) 1990, Sycamore Microsystems         �
;  �                      All rights reserved.                   �
;  聃컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�}

; Common EQUates used by Object Comm assembly modules.

;--- UART register offset mnemonics.
RBR	EQU	0
THR	EQU	0
IER	EQU	1
IIR	EQU	2
LCR	EQU	3
MCR	EQU	4
LSR	EQU	5
MSR	EQU	6


;--- Layer class fields, see OcLayer.PAS.
MACRO	LayerFields

 LType		db	16 DUP (?)
 LClass		db	16 DUP (?)
 LStat		dw	?
 LError		dw	?
 ErrHand	dd	?

 Upper		dd	?
 Lower		dd	?

 VMT		dw	?

ENDM


;--- RingBuf class fields, see OcRing.PAS.
MACRO	RingBufFields

 RAlloc		db	?
 RBuf		dd	?
 RHeadPtr	dd	? ; Actually, RHead (dw) and RSeg (dw).
 ROfs		dw	?
 RSize		dw	?
 RLimit		dw	?
 RTail		dw	?
 RLevel		dw	?

 TAlloc		db	?
 TBuf		dd	?
 TTailPtr	dd	? ; Actually, TTail (dw) and TSeg (dw).
 TOfs		dw	?
 TSize		dw	?
 TLimit		dw	?
 THead		dw	?
 TLevel		dw	?

ENDM


;--- ComPort class fields, see OcAsync.PAS.
MACRO	ComPortFields

 OldIsr		dd 	?
 NewIsr		dd	?
 IsrSize	dw	?
 JumpTable	dw	8 DUP (?)

 UART		db	?
 FIFOs		db	?
 FIFOLvl        db      ?
 FIFOLoop       dw      ?

 Base		dw	?
 IntNum		db	?
 Base8259	dw	?
 Chained	db	?

 UseFlow	db	?
 FlowHand	dd	?
 RPaused	db	?
 TPaused	db	?
 RFiltOn	db	?
 RFiltChar	db	32 DUP (?)
 RLevelOn	db	?
 RHiLevel	dw	?

 ; ... More stuff that we don't need...

ENDM


; The RingBuf Class structure
STRUC	RingBuf

 LayerFields
 RingBufFields

ENDS	RingBuf

; The ComPort class structure.
STRUC	ComPort

 LayerFields
 RingBufFields
 ComPortFields

ENDS	ComPort
