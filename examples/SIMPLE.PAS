program SimpleTerm;

uses {$I CrtUnit.inc}, OcLayers, OcAsync, OcUtils;

var
  Async   : ComPort;
  Rc, Tr  : char;
  N       : word;

begin
  if not Async.Init(1) then begin
    Writeln('Async.Init failed...');
    Halt(1);
  end; { then }

  Async.ComOptions(2400, 8, NoParity, 1, 0);

  {- Activate ComPort and turn on DTR and RTS signals -}
  if not Async.Activate then begin
    Async.Done;
    Writeln('Activate failed with error #', Async.LayerError);
    Halt(1);
  end; { then }
  Async.WriteUart(MCR, Async.ReadUart(MCR) or DtrOn or RtsOn);


  {- Use Async object -}
  ClrScr;
  Tr := ' ';
  while Tr <> Esc do begin

    Async.Process;

    {- Process keystrokes -}
    while KeyPressed and (Async.TranFree > 0) do begin
      Tr := ReadKey;
      Async.TranChar(Tr);
    end; { while }

    {- Process incoming data -}
    while Async.RecvLevel > 0 do Write(Async.RecvChar);
  end; { while }

  {- Shut down -}
  repeat Async.Process until Async.LayerStatus < LysBusy;
  Async.Deactivate;
  Async.Done;
  ClrScr;
end.

