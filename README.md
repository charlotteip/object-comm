# Object Comm

Object Comm is an [Object Pascal](https://en.wikipedia.org/wiki/Object_Pascal) serial communications library for [IBM PC-compatible](https://en.wikipedia.org/wiki/IBM_PC_compatible) microcomputers. I developed and sold Object Comm as part of my Sycamore Microsystems software consulting practice from around 1990-1993. It was my first commercial software product, advertised and sold mostly through tiny ads in the back of programming magazines like [Byte](https://en.wikipedia.org/wiki/Byte_(magazine)), [Dr. Dobbs Journal](https://en.wikipedia.org/wiki/Dr._Dobb%27s_Journal) and Jeff Duntemann's excellent [PC Techniques](http://www.duntemann.com/vdmarchive.htm) magazine. I ran across the floppies and a printed copy of the manual today and decided that Git[hu|la]b would be a great place to store the bits, after slapping an MIT OS license on it. 

You may be amused to discover that Object Pascal was possibly the first widely used object oriented programming language, thanks mostly to [Phillippe Kahn's pricing model](https://en.wikipedia.org/wiki/Philippe_Kahn#Borland_.281982.E2.80.931994.29:_compilers_and_tools) (cheap!) and the unfair advantage that it enjoyed in the market since programmers and non-programmers alike would buy the product simply to use it's excellent text editor, which it inherited from [Turbo Pascal](https://en.wikipedia.org/wiki/Turbo_Pascal).  

You'll find a typical [x86 Turbo Assembler](https://en.wikipedia.org/wiki/Turbo_Assembler) and Pascal [interrupt handler](https://en.wikipedia.org/wiki/Interrupt_request_(PC_architecture)) implementation for the venerable [8250/16550 UART family](https://en.wikipedia.org/wiki/16550_UART) (see [src/OCASYNC.ASM](/src/OCASYNC.ASM) and [src/OCASYNC.PAS](/src/OCASYNC.PAS)). The 16550 was the most popular FIFOed UART since it drove pretty much every PC serial, or ["COM"  port](https://en.wikipedia.org/wiki/Serial_port). There are also implementations of the popular modem protocols of the day like [X/YMODEM](https://en.wikipedia.org/wiki/XMODEM) (see [src/OCXMODEM.PAS](/src/OCXMODEM.PAS)) and [ZMODEM](https://en.wikipedia.org/wiki/ZMODEM) (see [src/OCZMODEM.PAS](/src/OCZMODEM.PAS)). 

[Bit-banging](https://en.wikipedia.org/wiki/Bit_banging) the PC's parallel, or ["LPT" port](https://en.wikipedia.org/wiki/Parallel_port) was a popular way to hack an external bus interface so there is an interface class for it ([src/OCLPT.PAS](/src/OCLPT.PAS)). An [FTP](https://en.wikipedia.org/wiki/File_Transfer_Protocol) implementation rounds out the collection ([src/OCFTP.PAS](/src/OCFTP.PAS)).

Oh, and those weird characters in the header comment blocks? I got cute and used the IBM PC extended character set, AKA ["Code Page 437"](https://en.wikipedia.org/wiki/Code_page_437) to make pretty boxes. In hindsight, perhaps I should have stuck with plain old ASCII.

If you programmed in the era of modem protocols, I hope you enjoy the trip down memory lane. If not, it turns out that your Raspberry Pi still has a [16550-ish UART](https://www.raspberrypi.org/wp-content/uploads/2012/02/BCM2835-ARM-Peripherals.pdf) hanging around, although finding a Pascal compiler for it might be a challenge!

Cheers,

[Rick Terrell](https://www.linkedin.com/in/terrell)
March 22, 2016


![Alt text](/floppy.jpg?raw=true "Object Comm")

![Alt text](/manual-cover.jpg?raw=true "Object Comm")
